//
//  Notifications+NotificationsSettable.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 1/29/18.
//  Copyright © 2018 Yerassyl Zhassuzakhov. All rights reserved.
//

import UserNotifications

protocol NotificationsSettable {
    func scheduleNotifications()
}

extension NotificationsSettable {
    func scheduleNotifications() {
        let defaults = UserDefaults.standard
        if let isNotificationEnabled = defaults.value(forKey: "notifications") as? Bool {
            switch isNotificationEnabled {
            case true:
                let content = UNMutableNotificationContent()
                content.title = "Тұрақты ескертпе".localize()
                content.categoryIdentifier = "alarm"
                content.userInfo = ["customData": "fizbuzz"]
                content.sound = UNNotificationSound.default
                content.body = "Көркем есімдерді жаттауды ұмытпаңыз".localize()
                content.badge = 1
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 61, repeats: true)
                let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request) { (error) in
                    if error != nil {
                        print(error)
                    }
                }
            case false:
                return
            }
        }
    }
}
