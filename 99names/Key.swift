//
//  Key.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 5/9/19.
//  Copyright © 2019 Yerassyl Zhassuzakhov. All rights reserved.
//

import Foundation

struct Key {
    static let currentThemeId = "currentThemeId"
    static let currentThemeDidChange = "currentThemeDidChange"
}

extension Notification.Name {
    static let currentThemeDidChange = Notification.Name(Key.currentThemeDidChange)
}
