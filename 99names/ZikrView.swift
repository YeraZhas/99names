//
//  ZikrView.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/16/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit
import SnapKit

class ZikrView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        layer.borderColor = Colors.borderGray.cgColor
        layer.borderWidth = 1.4
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.text = "Зікір етудің пайдалары"
        label.textColor = UserManager.currentThemeConfiguration.mainColor
        return label
    }()
    
    let benefitsLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = Colors.labelsColor
        label.numberOfLines = 0
        return label
    }()
    
    private func setupViews() {
        backgroundColor = .white
        layer.cornerRadius = 10
        
        addSubview(titleLabel)
        addSubview(benefitsLabel)
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(snp.top).offset(15)
            make.left.equalTo(snp.left).offset(7)
            make.right.equalTo(snp.right).offset(-7)
            make.bottom.equalTo(benefitsLabel.snp.top).offset(-15)
        }
        
        benefitsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(15)
            make.left.equalTo(snp.left).offset(7)
            make.right.equalTo(snp.right).offset(-7)
            make.bottom.equalTo(snp.bottom).offset(-15)
        }
        
    }

}
