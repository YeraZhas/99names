//
//  ShadowShowable.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 1/29/18.
//  Copyright © 2018 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

protocol ShadowShowable {
    func setupShadow()
}

extension ShadowShowable where Self: UIView {
    func setupShadow() {
        self.layer.shadowOpacity = 0.6
        self.layer.masksToBounds = false
        self.clipsToBounds = true
        self.layer.shadowRadius = 2
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowColor = Colors.mainBlue.cgColor
        self.layer.shadowPath = UIBezierPath(rect: self.layer.bounds).cgPath
    }
}

extension ShadowShowable where Self: UICollectionViewCell {
    func setupShadow() {
        self.contentView.layer.cornerRadius = 4
        self.contentView.layer.masksToBounds = true
        
        layer.shadowOpacity = 0.8
        layer.masksToBounds = false
        layer.shadowRadius = 2
        layer.shadowOffset = CGSize.zero
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowPath = UIBezierPath(rect: self.layer.bounds).cgPath
    }
}
