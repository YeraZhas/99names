//
//  UserManager.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 5/9/19.
//  Copyright © 2019 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class UserManager {
    static var currentThemeConfiguration: ThemeConfiguration {
        return getCurrentThemeConfiguration()
    }
    private static let userDefaults = UserDefaults.standard
    
    static func getCurrentThemeConfiguration() -> ThemeConfiguration {
        if let currentThemeId = userDefaults.string(forKey: Key.currentThemeId) {
            guard let theme = Theme.init(rawValue: currentThemeId) else {
                return BlueThemeConfigurationImpl()
            }
            
            return ThemeConfigurationFactory().makeThemeConfiguration(for: theme)
        }
        
        return BlueThemeConfigurationImpl()
    }
    
    private static func getCurrentTheme() -> Theme {
        if let currentThemeId = userDefaults.string(forKey: Key.currentThemeId) {
            guard let theme = Theme.init(rawValue: currentThemeId) else {
                return .blue
            }
            
            return theme
        }
        
        return .blue
    }
    
    static func refreshCurrentTheme() {
        setCurrentTheme(to: getCurrentTheme())
    }
    
    static func setCurrentTheme(to theme: Theme) {
        userDefaults.set(theme.rawValue, forKey: Key.currentThemeId)
        UINavigationBar.appearance().barTintColor = currentThemeConfiguration.mainColor
        UISwitch.appearance().onTintColor = currentThemeConfiguration.mainColor
        NotificationCenter.default.post(name: .currentThemeDidChange, object: nil)
        guard let currentView = UIApplication.shared.keyWindow?.rootViewController?.view else { return }
        let superView = currentView.superview
        currentView.removeFromSuperview()
        superView?.addSubview(currentView)
    }
}


