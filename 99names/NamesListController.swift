//
//  NamesListController.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 6/12/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit
import AVFoundation

let NamesListControllerNamesCellId = "bigCell"

class NamesListController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, NotificationsSettable {

    var menuTypeButton: UIBarButtonItem!
    var listTypeButton: UIBarButtonItem!
    var gridLayout = UICollectionViewFlowLayout()
    var listLayout = UICollectionViewFlowLayout()
    var isListLayoutSet = true
    var isGridLayoutSet = false
    let defaults = UserDefaults.standard
    var names = [Name]()
    var audioPlayer: AVAudioPlayer!
    var namesRequestManager: RequestManager!
    var cellImagesCache = [String: UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        preloadData()
        guard let _ = defaults.value(forKey: "lang") else {
            requestForLanguage()
            return
        }
        
        scheduleNotifications()
    }
    
    func preloadData() {
        namesRequestManager = NamesRequestManager()
        namesRequestManager.makeSimpleRequest(toResource: "names") { (namesArray) in
            guard let names = namesArray as? [Name] else { return }
            DispatchQueue.main.async {
                self.names = names
                self.collectionView.reloadData()
            }
        }
    }
    
    private func downloadImageWithUrl(url: String, completion: @escaping (UIImage?) -> Void) {
        DispatchQueue.global().async {
            guard let image = UIImage(named: url) else {
                completion(nil)
                return
            }
            DispatchQueue.main.async {
                self.cellImagesCache[url] = image
                completion(image)
            }
        }
    }
    
    func setupViews() {
        navigationItem.title = "Аллаһтың есімдері".localize()
        view.backgroundColor = .white
        view.addSubview(collectionView)
        collectionView.backgroundColor = Colors.backgroundGray
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(NamesBigCell.self, forCellWithReuseIdentifier: NamesListControllerNamesCellId)
        collectionView.contentInset = UIEdgeInsets(top: 7, left: 5, bottom: 8, right: 5)
        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.top)
            make.left.equalTo(view.snp.left)
            make.bottom.equalTo(view.snp.bottom)
            make.right.equalTo(view.snp.right)
        }
        menuTypeButton = UIBarButtonItem(image: UIImage(named: "menu-type"), style: .plain, target: self,
                                         action: #selector(gridTypeButtonPressed))
        listTypeButton = UIBarButtonItem(image: UIImage(named: "list-type"), style: .plain, target: self,
                                         action: #selector(listTypeButtonPressed))
        
        setupInitialLayout()
    }
    
    func setupInitialLayout() {
        if (isListLayoutSet) {
            listLayout.itemSize = CGSize(width: view.frame.width - 25, height: 200)
            collectionView.setCollectionViewLayout(listLayout, animated: true)
            navigationItem.rightBarButtonItem = menuTypeButton
        } else {
            gridLayout.itemSize = CGSize(width: view.frame.width / 2 - 10, height: (view.frame.width / 2 - 10) * 1.3)
            collectionView.setCollectionViewLayout(gridLayout, animated: true)
            navigationItem.rightBarButtonItem = listTypeButton
        }
        
    }
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return names.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NamesListControllerNamesCellId, for: indexPath) as! NamesBigCell
        cell.arabicNameLabel.text = names[indexPath.item].nameArabic
        cell.titleLabel.text = "\(indexPath.item + 1). \(names[indexPath.item].name)"
        cell.shareButton.tag = indexPath.item
        cell.shareButton.addTarget(self, action: #selector(shareName(sender:)), for: .touchUpInside)
        cell.playPronunciationButton.tag = indexPath.item.changeBy(+1)
        cell.playPronunciationButton.addTarget(self, action: #selector(playPronunciation(sender:)), for: .touchUpInside)
        let urlString = names[indexPath.item].image
        if let image = cellImagesCache[urlString] {
            cell.nameImage.image = image
            cell.loader.stopAnimating()
            cell.loader.removeFromSuperview()
            return cell
        }
        cell.loader.startAnimating()
        downloadImageWithUrl(url: urlString) { (image) in
            if (image != nil) {
                self.cellImagesCache[urlString] = image
                DispatchQueue.main.async {
                    self.collectionView.reloadItems(at: [indexPath])
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = NameDetailsController()
        vc.name = names[indexPath.item]
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func gridTypeButtonPressed() {
        navigationItem.rightBarButtonItem = listTypeButton
        UIView.animate(withDuration: 0.2) {
            self.isListLayoutSet = false
            self.gridLayout.itemSize = CGSize(width: self.collectionView.frame.width / 2 - 10,
                                              height: (self.collectionView.frame.width / 2 - 10) * 1.3)
            self.collectionView.collectionViewLayout.invalidateLayout()
            self.collectionView.setCollectionViewLayout(self.gridLayout, animated: true)
        }
        collectionView.reloadData()
    }
    
    @objc func listTypeButtonPressed() {
        navigationItem.rightBarButtonItem = menuTypeButton
        UIView.animate(withDuration: 0.2) {
            self.isGridLayoutSet = false
            self.listLayout.itemSize = CGSize(width: self.collectionView.frame.width - 25, height: 200)
            self.collectionView.collectionViewLayout.invalidateLayout()
            self.collectionView.setCollectionViewLayout(self.listLayout, animated: true)
            let indexPath = IndexPath(item: 0, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: .top, animated: false)
        }
        collectionView.reloadData()
    }
    
    // MARK: - Alert controller for language request
    
    func requestForLanguage() {
        let alert = UIAlertController(title: "Тілді таңдаңыз", message: nil, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Қазақша", style: .default) { (_) in
            self.defaults.set(0, forKey: "lang")
            self.defaults.synchronize()
            let vc = SideViewController(style: .grouped)
            vc.tableView.reloadData()
        }
        let action2 = UIAlertAction(title: "Русский", style: .default) {
            (_) in
            self.defaults.set(1, forKey: "lang")
            self.defaults.synchronize()
            let vc = SideViewController(style: .grouped)
            vc.tableView.reloadData()
        }
        alert.addAction(action1)
        alert.addAction(action2)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func shareName(sender: UIButton) {
        let meaning = names[sender.tag].meaningKz
        let text = "\(sender.tag + 1)-" + "есім".localize() + ". " + "\(names[sender.tag].name). " + meaning
        let link = "Жүктеп алу".localize() + " http://itunes.apple.com/app/id1273554579"
        if let image = UIImage(named: names[sender.tag].image) {
            let activityController = UIActivityViewController(activityItems: [text, link, image], applicationActivities: nil)
            activityController.excludedActivityTypes = [
                UIActivity.ActivityType.airDrop
            ]
            present(activityController, animated: true, completion: nil)
        }
        
    }
    
    @objc func playPronunciation(sender: UIButton) {
        AudioMediaManager().playMedia(withName: String(sender.tag), format: MediaFormats.mp3.rawValue) { (player) in
            self.audioPlayer = player
            self.audioPlayer.prepareToPlay()
            self.audioPlayer.play()
        }
    }
    
}










