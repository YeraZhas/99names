//
//  AppDelegate.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 6/12/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit
import SideMenuController
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let defaults = UserDefaults.standard
    let sideMenuViewController = SideMenuController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupWindow()
        setupSideMenuController()
        setupNavigationBarAndTheme()
        setupRootViewController()
        
        return true
    }
    
    private func setupWindow() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
    }
    
    private func setupSideMenuController() {
        SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu-button")
        SideMenuController.preferences.drawing.sidePanelPosition = .overCenterPanelLeft
        SideMenuController.preferences.drawing.sidePanelWidth = 260
        SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.animating.statusBarBehaviour = .horizontalPan
        SideMenuController.preferences.animating.transitionAnimator = FadeAnimator.self
    }
    
    private func setupNavigationBarAndTheme() {
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().tintColor = .white
        UIApplication.shared.applicationIconBadgeNumber = 0
        UserManager.refreshCurrentTheme()
    }
    
    private func setupRootViewController() {
        let sideController = SideViewController(style: .grouped)
        if let _ = defaults.value(forKey: "notifications") as? Bool {
            
        } else {
            setNotificationsAuthorization()
        }
        
        let navC = UINavigationController(rootViewController: NamesListController())
        navC.navigationItem.backBarButtonItem = UIBarButtonItem(title: "jnsdj", style: .plain, target: nil, action: nil)
        navC.interactivePopGestureRecognizer?.isEnabled = false
        sideMenuViewController.embed(sideViewController: sideController)
        sideMenuViewController.embed(centerViewController: navC)
        window?.rootViewController = sideMenuViewController
    }
    
    private func setNotificationsAuthorization() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            if granted {
                UserDefaults.standard.set(false, forKey: "notifications")
            } else {
                UserDefaults.standard.set(false, forKey: "notifications")
            }
        }
    }
    
//    func checkAuthoizationStatus() {
//        UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { (settings) in
//            if settings.authorizationStatus == .authorized {
//                self.defaults.set(true, forKey: "notifications")
//            } else {
//                self.defaults.set(false, forKey: "notifications")
//            }
//        })
//    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus != .authorized {
                self.defaults.set(false, forKey: "notifications")
            }
        }
        
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
