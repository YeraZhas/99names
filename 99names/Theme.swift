//
//  Theme.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 5/9/19.
//  Copyright © 2019 Yerassyl Zhassuzakhov. All rights reserved.
//

import Foundation

enum Theme: String {
    case blue = "blue"
    case green = "green"
    case orange = "orange"
    case purple = "purple"
}
