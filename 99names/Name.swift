//
//  Name.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/14/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import Foundation

struct Name {
    var name: String
    var nameArabic: String
    var meaningKz: String
    var meaningRu: String
    var zikr: String
    var ayats: [Ayat]
    var image: String
    
    init() {
        self.name = ""
        self.nameArabic = ""
        self.meaningKz = ""
        self.meaningRu = ""
        self.zikr = ""
        self.ayats = [Ayat]()
        self.image = ""
    }
    
}

