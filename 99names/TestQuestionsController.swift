//
//  TestQuestionsController.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/23/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit
import AVFoundation

class TestQuestionsController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var questions = [Question]()
    var score = 0
    var currentQuestion = 0
    let correctSound = Bundle.main.url(forResource: "correct", withExtension: "wav")
    let incorrectSound = Bundle.main.url(forResource: "incorrect", withExtension: "wav")
    var audioPlayer: AVAudioPlayer!

    override func viewDidLoad() {
        super.viewDidLoad()
        questions.shuffled()
        setupViews()
        changeNameView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    let progressView: UIProgressView = {
        let pv = UIProgressView(progressViewStyle: .bar)
        pv.trackTintColor = .white
        pv.progressTintColor = UserManager.currentThemeConfiguration.paleMainColor
        return pv
    }()
    
    let nameView: NameView = {
        let nv = NameView(frame: .zero)
        nv.blurView.removeFromSuperview()
        return nv
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    
    func setupViews() {
        view.backgroundColor = Colors.backgroundGray
        view.addSubview(progressView)
        view.addSubview(nameView)
        view.addSubview(collectionView)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = view.backgroundColor
        collectionView.register(TestQuestionCell.self, forCellWithReuseIdentifier: "cellId")
        collectionView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        
        progressView.progress = 0.0
        
        progressView.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.top)
            make.left.equalTo(view.snp.left)
            make.height.equalTo(20)
            make.right.equalTo(view.snp.right)
        }
        
        nameView.snp.makeConstraints { (make) in
            make.top.equalTo(progressView.snp.bottom).offset(7)
            make.left.equalTo(view.snp.left).offset(7)
            make.height.equalTo(view.frame.height / 3)
            make.right.equalTo(view.snp.right).offset(-7)
        }
        
        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(nameView.snp.bottom).offset(10)
            make.left.equalTo(view.snp.left).offset(7)
            make.right.equalTo(view.snp.right).offset(-7)
//            make.height.equalTo(230)
            make.bottom.equalTo(view.snp.bottom).offset(-10)
        }
        
    }
    
// MARK: - CollectionView delegate, dataSource methods implementation
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 10, height: (collectionView.frame.height - 40) / 4)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! TestQuestionCell
        cell.titleLabel.text = questions[currentQuestion].options[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.isUserInteractionEnabled = false
        currentQuestion += 1
        setProgressBar()
        let selectedCell = collectionView.cellForItem(at: indexPath) as! TestQuestionCell
        checkAnswers(cell: selectedCell)
        
        if currentQuestion >= questions.count {
            currentQuestion = 0
            showTestResult()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.refresh()
            self.getNextQuestion()
            self.collectionView.isUserInteractionEnabled = true
        }
    }

// MARK: - Test methods, including restartTest(), showTestResult(), changeNameView(), getNextQuestion(), checkAnswers()setProgressBar(), refresh()  methods
    
    func restartTest() {
        currentQuestion = 0
        score = 0
        progressView.progress = 0.0
    }
    
    func showTestResult() {
        let alertController = UIAlertController(title: "Құттықтаймыз!".localize(), message: "Тест бітті. Ұпай саны: ".localize() + "\(score)/\(questions.count)", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
            self.navigationController?.popToViewController((self.navigationController?.viewControllers[0])!, animated: true)
        }
        let restartAction = UIAlertAction(title: "Қайта бастау".localize(), style: .default) { (_) in
            self.restartTest()
        }
        alertController.addAction(okAction)
        alertController.addAction(restartAction)
        present(alertController, animated: true, completion: nil)
    }

    
    func changeNameView() {
        nameView.image = UIImage(named: questions[currentQuestion].image)
        nameView.nameLabel.text = "\(currentQuestion + 1). \(questions[currentQuestion].question)"
        nameView.arabicNameLabel.text = questions[currentQuestion].arabic
    }
    
    func getNextQuestion() {
        changeNameView()
        self.collectionView.reloadData()
    }
    
    func checkAnswers(cell: TestQuestionCell) {
        
        if questions[currentQuestion - 1].answer.contains(cell.titleLabel.text!) {
            cell.contentView.backgroundColor = UserManager.currentThemeConfiguration.rightAnswerColor
            score += 1
            playCorrectSound()
        } else {
            playIncorrectSound()
            cell.contentView.backgroundColor = UserManager.currentThemeConfiguration.wrongAnswerColor
            for i in 0..<4 {
                let index = IndexPath(item: i, section: 0)
                let newCell = collectionView.cellForItem(at: index) as! TestQuestionCell
                if questions[currentQuestion - 1].answer.contains(newCell.titleLabel.text!) {
                    newCell.contentView.backgroundColor = UserManager.currentThemeConfiguration.rightAnswerColor
                }
                
            }
        }
    }
    
    func setProgressBar() {
        progressView.setProgress(Float(currentQuestion) / Float(questions.count), animated: true)
    }
    
    func refresh() {
        for i in 0..<4 {
            let idx = IndexPath(item: i, section: 0)
            let clearedCell = collectionView.cellForItem(at: idx) as! TestQuestionCell
            clearedCell.contentView.backgroundColor = .white
        }
    }
    
    func playCorrectSound() {
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: correctSound!)
            audioPlayer.prepareToPlay()
            audioPlayer.playSound()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func playIncorrectSound() {
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: incorrectSound!)
            audioPlayer.prepareToPlay()
            audioPlayer.playSound()
        } catch {
            print(error.localizedDescription)
        }
    }
    
}




















