//
//  SegmentedControl.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/18/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class SegmentedControl: UIControl{
    
    private var labels = [UILabel]()
    var thumbView = UIView()
    
    var items: [String] = ["item 1", "item 2", "item 3"] {
        didSet {
            setupLabels()
        }
    }
    
    var selectedIndex: Int = 0 {
        didSet {
            displayNewSelectedIndex()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        var selectedFrame = self.bounds
        let newWidth = selectedFrame.width / CGFloat(items.count)
        selectedFrame.size.width = newWidth
        thumbView.frame = selectedFrame
        thumbView.backgroundColor = .red
        thumbView.layer.cornerRadius = thumbView.frame.height / 2
        
        let labelHeight = self.bounds.height
        let labelWidth = self.bounds.width / CGFloat(labels.count)
        
        for index in 0...labels.count - 1 {
            var label = labels[index]
            
            let xPosition = CGFloat(index) * labelWidth
            label.frame = CGRect(x: xPosition, y: 0, width: labelWidth, height: labelHeight)
        }
        
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let location = touch.location(in: self)
        var calculatedIndex: Int?
        
        for (index, item) in labels.enumerated() {
            if item.frame.contains(location) {
                calculatedIndex = index
            }
        }
        
        if calculatedIndex != nil {
            selectedIndex = calculatedIndex!
            sendActions(for: .valueChanged)
        }
        
        return false
        
    }
    
    func setupViews() {
        layer.cornerRadius = self.frame.height / 2
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 2
        
        backgroundColor = .clear
        
        setupLabels()
        insertSubview(thumbView, at: 0)
    }
    
    func displayNewSelectedIndex() {
        var label = labels[selectedIndex]
        self.thumbView.frame = label.frame
    }
    
    func setupLabels() {
        for label in labels {
            label.removeFromSuperview()
        }
        labels.removeAll(keepingCapacity: true)
        
        for index in 1...items.count {
            let label = UILabel(frame: .zero)
            label.text = items[index - 1]
            label.textAlignment = .center
            label.textColor = .black
            self.addSubview(label)
            labels.append(label)
        }
        
    }
    
}
