//
//  LocalNotificationsHelper.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/28/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit
import UserNotifications

class LocalNotificationsHelper: NSObject {

    func checkNotificationsEnabled() -> Bool {
        var doesExist = true
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .denied {
                doesExist = false
            } else {
                doesExist = true
            }
        }
        return doesExist
    }
    
    func checkNotificationExists(identifier: String) -> Bool {
        var doesExist = false
        UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
            for request in requests {
                if request.identifier == identifier {
                    doesExist = true
                }
            }
        }
        return doesExist
    }
    
    func scheduleLocal(identifier: String, alertDate: Date, content: UNNotificationContent) {
        let notificationRequest = UNNotificationRequest(identifier: identifier, content: content, trigger: nil)
        UNUserNotificationCenter.current().add(notificationRequest, withCompletionHandler: nil)
    }
    
    func removeNotification(identifier: String) {
        UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
            for request in requests {
                if request.identifier == identifier {
                    UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifier])
                }
            }
        }
    }
    
    
    
}
