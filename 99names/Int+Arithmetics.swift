//
//  Int+Arithmetics.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 1/30/18.
//  Copyright © 2018 Yerassyl Zhassuzakhov. All rights reserved.
//

import Foundation

extension Int {
    func changeBy(_ number: Int) -> Int {
        return self + number
    }
}
