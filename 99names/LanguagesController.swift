//
//  LanguagesController.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 6/14/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class LanguagesController: UITableViewController {

    let langs = ["Қазақша", "Русский"]
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellId")
        tableView.isScrollEnabled = false
        navigationItem.title = "Тіл".localize()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return langs.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath)
        cell.textLabel?.text = langs[indexPath.row]
        
        if let lang = defaults.value(forKey: "lang") as? Int {
            if indexPath.row == lang {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
        }
        
        if indexPath.row == 0 {
            defaults.set(0, forKey: "lang")
            let secondCell = tableView.cellForRow(at: IndexPath(row: 1, section: 0))
            secondCell?.accessoryType = .none
        } else {
            defaults.set(1, forKey: "lang")
            let secondCell = tableView.cellForRow(at: IndexPath(row: 0, section: 0))
            secondCell?.accessoryType = .none
        }
        defaults.synchronize()
        if let vc = navigationController?.viewControllers[0] as? UITableViewController {
            vc.tableView.reloadData()
            vc.navigationItem.title = "Баптаулар".localize()
        }
        tableView.deselectRow(at: indexPath, animated: true)
        navigationItem.title = "Тіл".localize()
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
