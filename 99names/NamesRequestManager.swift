//
//  NamesManager.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 1/29/18.
//  Copyright © 2018 Yerassyl Zhassuzakhov. All rights reserved.
//

import Foundation

class NamesRequestManager: RequestManager {
    let queue = DispatchQueue(label: "namesQueue", qos: .background)
    func makeSimpleRequest(toResource resource: String, completion: @escaping  (AnyObject) -> Void) {
        queue.async {
            do {
                let path = Bundle.main.url(forResource: resource, withExtension: "json")!
                
                let data = try Data(contentsOf: path)
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [[String: AnyObject]]
                var names = [Name]()
                for item in json {
                    var name = Name()
                    name.ayats = [Ayat]()
                    name.meaningKz = item["meaningKz"] as! String
                    name.meaningRu = item["meaningRu"] as! String
                    name.name = item["name"] as! String
                    name.nameArabic = item["nameArabic"] as! String
                    name.zikr = item["zikr"] as! String
                    name.image = item["image"] as! String
                    if let ayats = item["ayats"] as? [[String: AnyObject]] {
                        for verse in ayats {
                            var ayat = Ayat()
                            ayat.ayat = verse["ayat"] as! String
                            ayat.ayatKz = verse["ayatKz"] as! String
                            ayat.order = verse["order"] as! String
                            name.ayats.append(ayat)
                        }
                    }
                    names.append(name)
                }
                completion(names as AnyObject)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}
