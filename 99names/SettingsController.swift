//
//  SettingsController.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 6/14/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit
import UserNotifications

class SettingsController: UITableViewController, UNUserNotificationCenterDelegate {
    
    let defaults = UserDefaults.standard
    let titles = ["Ескертпелер", "Тіл", "Дыбыс", "Достарыммен бөлісу", "Бағалау"]
    let langs = ["Қазақша", "Русский"]
    let keywords = ["notifications", "", "sounds"]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(SwitchCell.self, forCellReuseIdentifier: "switchCell")
        tableView.register(RightDetailCell.self, forCellReuseIdentifier: "cellId")
        tableView.register(ThemeSelectionCell.self, forCellReuseIdentifier: "themeCell")
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.isScrollEnabled = false
        navigationItem.title = "Баптаулар".localize()
        UNUserNotificationCenter.current().delegate = self
//        scheduleNotifications()
        defaults.addObserver(self, forKeyPath: "notifications", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    deinit {
        defaults.removeObserver(self, forKeyPath: "notifications")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        self.tableView.reloadData()
    }

// MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 1:
            return 1
        case 2:
            return 2
        default:
            fatalError()
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Негізгі".localize()
        case 1:
            return "Темы".localize()
        case 2:
            return "Қосымша".localize()
        default:
            fatalError()
        }
    }

//MARK: - Cell For Row method
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        if indexPath.section == 0 {
            switch indexPath.row {
            case let x where x == 0 || x == 2:
                let cell = SwitchCell()
                cell.textLabel?.text = titles[indexPath.section * 3 + indexPath.row].localize()
                cell.selectionStyle = .none
                cell.toggleSwitch.tag = x
                cell.toggleSwitch.addTarget(self, action: #selector(handleSwitchToggle(sender:)), for: .valueChanged)
                if let status = defaults.value(forKey: keywords[x]) as? Bool {
                    cell.toggleSwitch.isOn = status
                }
                return cell
            case 1:
                let cell = RightDetailCell()
                cell.textLabel?.text = titles[indexPath.section * 3 + indexPath.row].localize()
                if let lang = defaults.value(forKey: "lang") as? Int {
                    cell.detailTextLabel?.text = langs[lang]
                }
                return cell
            default:
                return UITableViewCell()
            }
        } else if indexPath.section == 2 {
            let cell = UITableViewCell()
            cell.textLabel?.text = titles[(indexPath.section - 1) * 3 + indexPath.row].localize()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "themeCell", for: indexPath) as! ThemeSelectionCell
            
            return cell
        }
    }

//MARK: - Handle UISwitch toogle method

    @objc func handleSwitchToggle(sender: UISwitch) {
        print(sender.tag)
        switch sender.tag {
        case 0:
            if sender.isOn == true {
            UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { (settings) in
                if settings.authorizationStatus == .authorized {
                    self.terminateNotifications()
                    self.defaults.set(true, forKey: "notifications")
                    self.scheduleNotifications()
                    UNUserNotificationCenter.current().getPendingNotificationRequests { (notif) in
                        print(notif.count)
                        for item in notif {
                            print(item.content)
                        }
                    }
                } else {
                    self.showEventAccessDeniedAlert() { 
                        sender.setOn(false, animated: true)
                    }
                }
            })
            } else {
                defaults.set(false, forKey: "notifications")
                terminateNotifications()
            }
        case 2:
            if sender.isOn == true {
                defaults.set(true, forKey: "sounds")
            } else {
                defaults.set(false, forKey: "sounds")
            }
        default:
            fatalError()
        }
        defaults.synchronize()
    }

    func showEventAccessDeniedAlert(completionHandler: @escaping () -> Void) {
        let alertController = UIAlertController(title: "Ескерту".localize(), message: "Ескертпелерге рұқсат берілген жоқ. ".localize(), preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Баптауларға кіру".localize(), style: .default) { (alertAction) in
            self.openSettings()
            completionHandler()
        }
        let cancelAction = UIAlertAction(title: "Болдыртпау".localize(), style: .cancel) { (alertAction) in
            completionHandler()
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    func openSettings() {
        if let appSettings = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(appSettings, options: [:], completionHandler: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
            switch indexPath.row {
            case 1:
                let vc = LanguagesController(style: .grouped)
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("")
            }
        } else if indexPath.section == 2 {
            switch indexPath.row {
            case 0:
                let text = "Аллаһтың көркем есімдерін жаттаyға арналған қосымша. http://itunes.apple.com/app/id1273554579"
                if let image = UIImage(named: "banner.png") {
                    let activityController = UIActivityViewController(activityItems: [text, image], applicationActivities: nil)
                    activityController.excludedActivityTypes = [
                        UIActivity.ActivityType.airDrop
                    ]
                    present(activityController, animated: true, completion: nil)
                }
            case 1:
                rateApp()
            default:
                print("")
            }
        }

    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 1 ? 104 : 44
    }
 
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    func terminateNotifications() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
    
    func scheduleNotifications() {
        let content = UNMutableNotificationContent()
        content.title = "Тұрақты ескертпе".localize()
        content.categoryIdentifier = "alarm"
        content.userInfo = ["customData": "fizbuzz"]
        content.sound = UNNotificationSound.default
        content.body = "Көркем есімдерді жаттауды ұмытпаңыз".localize()
        
        var dateInfo = DateComponents()
//        dateInfo.day = 3
        dateInfo.hour = 10
        dateInfo.minute = 40
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateInfo, repeats: true)
        let request = UNNotificationRequest(identifier: "id", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error) in
            if error != nil {
                print(error)
            }
        }
    }
    
    func rateApp() {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1273554579?action=write-review&mt=8") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
}
