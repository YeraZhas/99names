//
//  String+Localization.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 1/29/18.
//  Copyright © 2018 Yerassyl Zhassuzakhov. All rights reserved.
//

import Foundation

extension String {
    
    func localize() -> String {
        if let url = Bundle.main.url(forResource: "Localizable", withExtension: "strings"), let stringDict = NSDictionary(contentsOf: url) as? [String: String] {
            if let lang = UserDefaults.standard.value(forKey: "lang") as? Int {
                switch lang {
                case 1:
                    if let localizedString = stringDict[self] {
                        return localizedString
                    }
                    
                    return self
                case 0:
                    return self
                default:
                    return self
                }
            } else {
                return self
            }
        }
        fatalError()
    }
}
