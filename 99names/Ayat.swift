//
//  Ayat.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/14/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import Foundation

struct Ayat {
    var ayat: String
    var ayatKz: String
    var order: String
    
    init() {
        self.ayat = ""
        self.ayatKz = ""
        self.order = ""
    }
    
}
