//
//  Question.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/23/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import Foundation

struct Question {
    var question: String
    var options: [String]
    var answer: String
    var arabic: String
    var image: String
    
    init() {
        self.question = ""
        self.options = [String]()
        self.answer = ""
        self.arabic = ""
        self.image = ""
    }
    
}
