//
//  CustomLabel.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/17/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class CustomLabel: UILabel {

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 10)
        super.drawText(in: rect.inset(by: insets))
    }

}
