//
//  NamesBigCell.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 6/12/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class NamesBigCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var bounds: CGRect {
        didSet {
            contentView.frame = bounds
        }
    }
    
    override func prepareForReuse() {
        nameImage.image = nil
        super.prepareForReuse()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.masksToBounds = true
        layer.cornerRadius = 10
        layer.borderColor = UserManager.currentThemeConfiguration.borderColor.cgColor
        layer.borderWidth = 2
    }
    
    private func setupViews() {
        contentView.addSubview(nameImage)
        contentView.addSubview(blurView)
        contentView.addSubview(whiteView)
        contentView.addSubview(arabicNameLabel)
        contentView.addSubview(titleLabel)
        contentView.addSubview(shareButton)
        contentView.addSubview(playPronunciationButton)
        contentView.addSubview(loader)
        
        nameImage.snp.makeConstraints { (make) in
            make.top.equalTo(contentView.snp.top)
            make.left.equalTo(contentView.snp.left)
            make.bottom.equalTo(contentView.snp.bottom)
            make.right.equalTo(contentView.snp.right)
        }
        
        blurView.snp.makeConstraints { (make) in
            make.edges.equalTo(nameImage.snp.edges)
        }
        
        loader.snp.makeConstraints { (make) in
            make.centerX.equalTo(nameImage.snp.centerX)
            make.centerY.equalTo(nameImage.snp.centerY)
            make.height.equalTo(30)
            make.width.equalTo(30)
        }
        
        whiteView.snp.makeConstraints { (make) in
            make.bottom.equalTo(nameImage.snp.bottom)
            make.left.equalTo(nameImage.snp.left)
            make.right.equalTo(nameImage.snp.right)
            make.top.equalTo(titleLabel.snp.top).offset(-6)
        }
        
        arabicNameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nameImage.snp.top).offset(20)
            make.left.equalTo(nameImage.snp.left)
            make.right.equalTo(nameImage.snp.right)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(whiteView.snp.top).offset(6)
            make.right.equalTo(whiteView.snp.right).offset(-10)
            make.left.equalTo(whiteView.snp.left)
            make.bottom.equalTo(shareButton.snp.top).offset(-6)
        }
        
        shareButton.snp.makeConstraints { (make) in
            make.bottom.equalTo(whiteView.snp.bottom).offset(-7)
            make.right.equalTo(titleLabel.snp.right)
            make.width.equalTo(35)
            make.height.equalTo(35)
        }
        
        playPronunciationButton.snp.makeConstraints { (make) in
            make.bottom.equalTo(shareButton.snp.bottom)
            make.right.equalTo(shareButton.snp.left).offset(-8)
            make.height.equalTo(shareButton.snp.height)
            make.width.equalTo(shareButton.snp.width)
        }
        
    }
    
    let nameImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        
        return iv
    }()
    
    let blurView: UIView = {
        let view = UIView()
        view.backgroundColor = UserManager.currentThemeConfiguration.mainColor.withAlphaComponent(0.256)
        
        return view
    }()
    
    let whiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    let arabicNameLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(30)
        label.textColor = .white
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UserManager.currentThemeConfiguration.labelsColor
        label.text = "Ар-Рахман"
        label.numberOfLines = 0
        label.textAlignment = .right
        label.font = label.font.withSize(17)
        return label
    }()
    
    let shareButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "share-google"), for: .normal)
        button.tintColor = Colors.grayLabel
        return button
    }()
    
    let playPronunciationButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "play-small"), for: .normal)
        button.tintColor = .white
        button.backgroundColor = Colors.grayLabel
        button.layer.cornerRadius = 17.5
        return button
    }()
    
    let loader: UIActivityIndicatorView = {
        let iv = UIActivityIndicatorView(style: .gray)
        return iv
    }()
    
}
