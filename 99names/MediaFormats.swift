//
//  MediaFormats.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 1/29/18.
//  Copyright © 2018 Yerassyl Zhassuzakhov. All rights reserved.
//

import Foundation

enum MediaFormats: String {
    case mp3 = "mp3"
    case mp4 = "mp4"
}
