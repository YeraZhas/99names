//
//  MediaManager.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 1/29/18.
//  Copyright © 2018 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit
import AVFoundation

protocol MediaManager {
    func playMedia(withName name: String, format: String, completionHandler: @escaping (AVAudioPlayer) -> Void)
}
