//
//  SwitchCell.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 6/14/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class SwitchCell: UITableViewCell {
    
    let defaults = UserDefaults.standard
    
    override var bounds: CGRect {
        didSet {
            self.frame = bounds
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.accessoryView = toggleSwitch
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let toggleSwitch: UISwitch = {
        let sw = UISwitch(frame: .zero)
        
        return sw
    }()
    
    
}
