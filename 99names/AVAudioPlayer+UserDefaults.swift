//
//  AVAudioPlayer+UserDefaults.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 1/29/18.
//  Copyright © 2018 Yerassyl Zhassuzakhov. All rights reserved.
//

import AVFoundation

extension AVAudioPlayer {
    func playSound() {
        let defaults = UserDefaults.standard
        if let sound = defaults.value(forKey: "sounds") as? Bool {
            switch sound {
            case false:
                return
            case true:
                self.play()
            }
        } else {
            return
        }
    }
}
