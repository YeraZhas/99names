//
//  AudioManager.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 1/29/18.
//  Copyright © 2018 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit
import AVFoundation

class AudioMediaManager: MediaManager {
    func playMedia(withName name: String, format: String, completionHandler: @escaping (AVAudioPlayer) -> Void) {
        do {
            let audio = Bundle.main.url(forResource: name, withExtension: "m4a")!
            let audioPlayer = try AVAudioPlayer(contentsOf: audio)
            completionHandler(audioPlayer)
        } catch {
            print(error.localizedDescription)
        }
    }
}

