//
//  QuranVersesView.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/15/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class QuranVersesView: UIView {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        layer.borderColor = Colors.borderGray.cgColor
        layer.borderWidth = 1.4
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.text = "Құран аяттары"
        label.textColor = UserManager.currentThemeConfiguration.mainColor
        return label
    }()
    
    let versesStackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.distribution = .fill
        sv.spacing = 15
        return sv
    }()
    
    func setupViews() {
        backgroundColor = .white
        layer.cornerRadius = 10
        
        addSubview(titleLabel)
        addSubview(versesStackView)
        
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(snp.top).offset(15)
            make.left.equalTo(snp.left).offset(7)
            make.right.equalTo(snp.right).offset(-7)
            make.bottom.equalTo(versesStackView.snp.top).offset(-15)
        }
        
        versesStackView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(15)
            make.left.equalTo(snp.left)
            make.right.equalTo(snp.right)
            make.bottom.equalTo(snp.bottom).offset(-15)
        }
        
    }
    

}
