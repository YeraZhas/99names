//
//  BlueThemeConfigurationFactoryImpl.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 5/9/19.
//  Copyright © 2019 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

struct BlueThemeConfigurationImpl: ThemeConfiguration {
    var mainColor = UIColor.rgb(102, green: 219, blue: 250)
    var rightAnswerColor = UIColor.rgb(126, green: 226, blue: 246)
    var paleMainColor = UIColor.rgb(223, green: 242, blue: 255)
    var secondaryMaskColor = UIColor.rgb(255, green: 250, blue: 147).withAlphaComponent(0.4)
}

struct OrangeThemeConfigurationImpl: ThemeConfiguration {
    var mainColor = UIColor.rgb(250, green: 164, blue: 102)
    var rightAnswerColor = UIColor.rgb(240, green: 140, blue: 67)
    var paleMainColor = UIColor.rgb(255, green: 188, blue: 140)
    var secondaryMaskColor = UIColor.rgb(255, green: 215, blue: 186)
}

struct GreenThemeConfigurationImpl: ThemeConfiguration {
    var mainColor = UIColor.rgb(163, green: 203, blue: 55)
    var rightAnswerColor = UIColor.rgb(183, green: 223, blue: 75)
    var paleMainColor = UIColor.rgb(221, green: 255, blue: 130)
    var secondaryMaskColor = UIColor.rgb(194, green: 236, blue: 81)
}

struct PurpleThemeConfigurationImpl: ThemeConfiguration {
    var mainColor = UIColor.rgb(139, green: 120, blue: 230)
    var rightAnswerColor = UIColor.rgb(134, green: 120, blue: 201)
    var paleMainColor = UIColor.rgb(173, green: 156, blue: 255)
    var secondaryMaskColor = UIColor.rgb(170, green: 152, blue: 255)
}
