//
//  NameDetailsController.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/15/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit
import AVFoundation

class NameDetailsController: UIViewController {
    
    var name = Name()
    var verses = [VerseView]()
    var audioPlayer: AVAudioPlayer!

    override func viewDidLoad() {
        super.viewDidLoad()
        for _ in 0..<name.ayats.count {
            let verseView = VerseView(frame: .zero)
            verses.append(verseView)
        }
        for i in 0..<verses.count {
            verses[i].versesLabel.text = "\(name.ayats[i].ayat)"
            verses[i].meaningLabel.text = name.ayats[i].ayatKz
            verses[i].orderLabel.text = name.ayats[i].order
            quranVersesView.versesStackView.insertArrangedSubview(verses[i], at: i)
        }
        setupViews()
        guard name.ayats.count != 0 else {
            return
        }
        
        nameView.arabicNameLabel.text = name.nameArabic
        nameView.nameLabel.text = name.name
        nameView.image = UIImage(named: name.image)
        nameView.meaningLabel.text = name.meaningKz
        zikrView.benefitsLabel.text = name.zikr
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        customScrollView.contentSize.height = nameView.frame.height + quranVersesView.frame.height + zikrView.frame.height + 35
    }

    let nameView = NameView(frame: .zero)
    
    let quranVersesView = QuranVersesView(frame: .zero)
    
    let zikrView = ZikrView(frame: .zero)
    
    let customScrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.isScrollEnabled = true
        sv.showsVerticalScrollIndicator = false
        return sv
    }()
    
    let playButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UserManager.currentThemeConfiguration.mainColor
        button.setImage(UIImage(named: "play"), for: .normal)
        button.layer.cornerRadius = 30
        button.tintColor = .white
        return button
    }()
    
    func setupViews() {
        view.backgroundColor = Colors.backgroundGray
        view.addSubview(customScrollView)
        customScrollView.addSubview(nameView)
        customScrollView.addSubview(quranVersesView)
        customScrollView.addSubview(zikrView)
        customScrollView.addSubview(playButton)
        
        playButton.addTarget(self, action: #selector(playPronunciation(sender:)), for: .touchUpInside)
        
        customScrollView.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.top)
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
            make.bottom.equalTo(view.snp.bottom)
        }
        
        nameView.snp.makeConstraints { (make) in
            make.top.equalTo(customScrollView.snp.top).offset(7)
            make.left.equalTo(view.snp.left).offset(7)
            make.right.equalTo(view.snp.right).offset(-7)
            make.height.equalTo(view.frame.height / 3 + 40)
        }
        
        quranVersesView.snp.makeConstraints { (make) in
            make.top.equalTo(nameView.snp.bottom).offset(10)
            make.centerX.equalTo(view.snp.centerX)
            make.width.equalTo(view.frame.width - 14)
            
        }
        
        zikrView.snp.makeConstraints { (make) in
            make.top.equalTo(quranVersesView.snp.bottom).offset(10)
            make.left.equalTo(view.snp.left).offset(7)
            make.right.equalTo(view.snp.right).offset(-7)
            make.bottom.equalTo(customScrollView.snp.bottom).offset(-10)
        }
        
        playButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(nameView.whiteView.snp.top)
            make.right.equalTo(nameView.snp.right)
            make.width.equalTo(60)
            make.height.equalTo(60)
        }
    }
    
    @objc func playPronunciation(sender: UIButton) {
        AudioMediaManager().playMedia(withName: "1", format: MediaFormats.mp3.rawValue) { (player) in
            self.audioPlayer = player
            self.audioPlayer.prepareToPlay()
            self.audioPlayer.play()
        }
    }
}















