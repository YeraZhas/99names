//
//  TestController.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/23/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class TestSectionsController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    /*, "21 - 30", "31 - 40", "41 - 50", "51 - 60", "61 - 70", "71 - 80", "81 - 90", "91 - 99", "1 - 99"*/
    var gridLayout = UICollectionViewFlowLayout()
    var questions = [Question]()
    let intervals = [0...10, 11...21, 22...30, 31...40, 41...50,
                     51...60, 61...70, 71...80, 81...90, 91...99, 1...99]
    let sectionIntervals = ["1 - 10", "11 - 20", "21 - 30"]

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        loadQuestions()
    }

    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    func loadQuestions() {
        do {
            let path = Bundle.main.url(forResource: "test", withExtension: "json")!
            let data = try Data(contentsOf: path)
            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [[String: AnyObject]]
            
            for item in json {
                var question = Question()
                question.question = item["question"] as! String
                question.answer = item["answer"] as! String
                question.arabic = item["arabic"] as! String
                question.image = item["image"] as! String
                
                for option in item["options"] as! [[String: AnyObject]] {
                    question.options.append(option["option"] as! String)
                }
                
                questions.append(question)
            }
            
        } catch {
            print(error.localizedDescription)
        }
    }

    func setupViews() {
        view.backgroundColor = .white
        view.addSubview(collectionView)
        collectionView.backgroundColor = Colors.backgroundGray
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(TestSectionCell.self, forCellWithReuseIdentifier: "cellId")
        collectionView.contentInset = UIEdgeInsets(top: 7, left: 5, bottom: 0, right: 5)
        
        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.top)
            make.left.equalTo(view.snp.left)
            make.bottom.equalTo(view.snp.bottom)
            make.right.equalTo(view.snp.right)
        }
        setupLayout()
    }
    
    func setupLayout() {
        gridLayout.itemSize = CGSize(width: view.frame.width / 2 - 10, height: view.frame.width / 2 - 10)
        collectionView.setCollectionViewLayout(gridLayout, animated: false)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sectionIntervals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! TestSectionCell
        cell.intervalLabel.text = sectionIntervals[indexPath.item]
        cell.titleLabel.text = "Көркем есімдер".localize()
        cell.beginButton.tag = indexPath.item
        cell.beginButton.addTarget(self, action: #selector(beginButtonPressed(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func beginButtonPressed(sender: UIButton) {
        let testVC = TestQuestionsController()
        var portionOfQuestions = [Question]()
        
        for index in intervals[sender.tag] {
            portionOfQuestions.append(questions[index])
        }
        
        testVC.questions = portionOfQuestions
        self.navigationController?.pushViewController(testVC, animated: true)
    }
}













