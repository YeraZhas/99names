//
//  UIColor+ColorNames.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 1/29/18.
//  Copyright © 2018 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

struct Colors {
    static let mainBlue = /*UIColor.rgb(0, green: 230, blue: 118)*/ UIColor.rgb(89, green: 182, blue: 201)
    static let backgroundGray = UIColor.rgb(237, green: 237, blue: 237)
    static let blueMask = /*UIColor.rgb(124, green: 179, blue: 66)*/Colors.mainBlue.withAlphaComponent(0.40)
    static let grayLabel = UIColor.rgb(154, green: 151, blue: 151)
    static let labelsColor = UIColor.rgb(118, green: 115, blue: 115)
    static let wrongAnswerColor = UIColor.rgb(228, green: 228, blue: 228)
    static let rightAnswerColor = UIColor.rgb(126, green: 226, blue: 246)
    static let paleBlue = UIColor.rgb(223, green: 242, blue: 255)
    static let secondBlueMask = UIColor.rgb(255, green: 250, blue: 147).withAlphaComponent(0.4)
    static let borderGray = UIColor.rgb(216, green: 216, blue: 216)
}

extension UIColor {
    static func rgb(_ red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}

