//
//  ListLayout.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 6/13/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class ListLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLayout() {
        minimumLineSpacing = 1
        minimumInteritemSpacing = 0
        scrollDirection = .vertical
    }
    
    func itemWidth() -> CGFloat {
        return 375
    }
    
    override var itemSize: CGSize {
        set {
            self.itemSize = CGSize(width: itemWidth(), height: itemSize.height)
        }
        get {
            return CGSize(width: itemWidth(), height: self.itemSize.height)
        }
    }
}
