//
//  TestQuestionCell.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/23/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class TestQuestionCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        layer.masksToBounds = true
        layer.cornerRadius = 10
        layer.borderWidth = 1.4
        layer.borderColor = Colors.borderGray.cgColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    
    private func setupViews() {
//        contentView.layer.cornerRadius = 5
        contentView.backgroundColor = .white
        addSubview(titleLabel)
        
        titleLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(contentView.snp.centerY)
            make.left.equalTo(contentView.snp.left).offset(4)
            make.right.equalTo(contentView.snp.right).offset(-4)
        }
        
    }
    
}
