//
//  SideMenuController.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 6/12/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit
import SideMenuController

class SideViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        tableView.register(SideMenuCell.self, forCellReuseIdentifier: "cellId")
        tableView.isScrollEnabled = false
        tableView.separatorStyle = .none
        UserDefaults.standard.addObserver(self, forKeyPath: "lang", options: NSKeyValueObservingOptions.new, context: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(observeCurrentThemeChange), name: .currentThemeDidChange, object: nil)
    }
    
    deinit {
        UserDefaults.standard.removeObserver(self, forKeyPath: "lang")
    }
    
    @objc private func observeCurrentThemeChange() {
        tableView.reloadData()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        self.tableView.reloadData()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! SideMenuCell
            switch indexPath.row {
            case 0:
                cell.iconVIew.image = UIImage(named: "listIcon")
                cell.titleLabel.text = "Тізім".localize()
            case 1:
                cell.iconVIew.image = UIImage(named: "gridIcon")
                cell.titleLabel.text = "Кесте".localize()
            case 2:
                cell.iconVIew.image = UIImage(named: "testIcon")
                cell.titleLabel.text = "Тест".localize()
            case 3:
                cell.iconVIew.image = UIImage(named: "settingsIcon")
                cell.titleLabel.text = "Баптаулар".localize()
            default:
                fatalError()
            }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 200
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = SideMenuHeaderView(frame: CGRect(x: 0, y: 0, width: 350, height: 200))
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                let nc = UINavigationController(rootViewController: NamesListController())
                sideMenuController?.embed(centerViewController: nc)
            case 1:
                let vc = NamesListController()
                vc.isListLayoutSet = false
                let nc = UINavigationController(rootViewController: vc)
                sideMenuController?.embed(centerViewController: nc)
            case 2:
                let nc = UINavigationController(rootViewController: TestSectionsController())
                sideMenuController?.embed(centerViewController: nc)
            case 3:
                let nc = UINavigationController(rootViewController: SettingsController(style: .grouped))
                sideMenuController?.embed(centerViewController: nc)
            default:
                fatalError()
            }
        }
    }
}
