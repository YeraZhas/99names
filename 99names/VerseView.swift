//
//  VerseView.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/15/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class VerseView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let versesLabel: CustomLabel = {
        let label = CustomLabel()
        label.numberOfLines = 0
        label.font = label.font.withSize(25)
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .natural
        return label
    }()
    
    let meaningLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = Colors.labelsColor
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .left
        return label
    }()
    
    let orderLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .left
        label.textColor = Colors.labelsColor
        return label
    }()
    
    private func setupViews() {
        addSubview(versesLabel)
        addSubview(meaningLabel)
        addSubview(orderLabel)
        
        versesLabel.snp.makeConstraints { (make) in
            make.top.equalTo(snp.top).offset(10)
            make.left.equalTo(snp.left)
            make.right.equalTo(snp.right)
        }
        
        meaningLabel.snp.makeConstraints { (make) in
            make.top.equalTo(versesLabel.snp.bottom).offset(20)
            make.left.equalTo(snp.left).offset(7)
            make.right.equalTo(snp.right).offset(-7)
            make.height.equalTo(meaningLabel.snp.height)
        }
        
        orderLabel.snp.makeConstraints { (make) in
            make.top.equalTo(meaningLabel.snp.bottom).offset(20)
            make.left.equalTo(snp.left).offset(7)
            make.right.equalTo(snp.right).offset(-7)
            make.bottom.equalTo(snp.bottom)
        }
        
//        self.snp.makeConstraints { (make) in
//            make.top.equalTo(versesLabel.snp.top).offset(-10)
//            make.bottom.equalTo(orderLabel.snp.bottom).offset(10)
//        }
        
    }
    
}
