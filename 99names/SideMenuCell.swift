//
//  SideMenuCell.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/25/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.frame = self.bounds
        addSubview(iconVIew)
        addSubview(titleLabel)
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupViews()
    }
    
    private func setupViews() {
        iconVIew.snp.makeConstraints { (make) in
            make.top.equalTo(contentView.snp.top).offset(13)
            make.left.equalTo(contentView.snp.left).offset(13)
            make.bottom.equalTo(contentView.snp.bottom).offset(-11)
            make.width.equalTo(24)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(contentView.snp.centerY)
            make.height.equalTo(40)
            make.left.equalTo(iconVIew.snp.right).offset(35)
            make.width.equalTo(200)
        }
    }
    
    let iconVIew: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    let titleLabel: UILabel = {
        let title = UILabel()
        title.textAlignment = .left
        return title
    }()
    
}
