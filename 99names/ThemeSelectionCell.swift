//
//  ThemeSelectionCell.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 5/9/19.
//  Copyright © 2019 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

typealias ThemeCellData = (color: UIColor, theme: Theme)

class ThemeSelectionCell: UITableViewCell {
    private var layout = UICollectionViewFlowLayout()
    private let themeCellsData: [ThemeCellData] = [(BlueThemeConfigurationImpl().mainColor, .blue),
                                                   (OrangeThemeConfigurationImpl().mainColor, .orange),
                                                   (GreenThemeConfigurationImpl().mainColor, .green),
                                                   (PurpleThemeConfigurationImpl().mainColor, .purple)]
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupCollectionView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    private func setupCollectionView() {
        layout.itemSize = CGSize(width: 80, height: 80)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 10
        collectionView.setCollectionViewLayout(layout, animated: false)
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        collectionView.showsHorizontalScrollIndicator = false
    }
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        cv.backgroundColor = .white
        cv.register(ThemeCell.self, forCellWithReuseIdentifier: "themeCell")
        
        return cv
    }()
}

extension ThemeSelectionCell: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return themeCellsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "themeCell", for: indexPath)
        let themeCellData = themeCellsData[indexPath.item]
        cell.backgroundColor = themeCellData.color
        cell.layer.cornerRadius = 9
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let theme = themeCellsData[indexPath.item].theme
        UserManager.setCurrentTheme(to: theme)
    }
}
