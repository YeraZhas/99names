//
//  SideMenuHeaderView.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/25/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class SideMenuHeaderView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let logoLabel: UILabel = {
        let label = UILabel()
        label.text = "الله"
        label.font = label.font.withSize(120)
        label.textColor = .white
        label.textAlignment = .left
        return label
    }()
    
    let appNameLabel: UILabel = {
        let label = UILabel()
        label.text = "Аллаһтың көркем есімдері".localize()
        label.textColor = .white
        label.textAlignment = .left
        return label
    }()
    
    private func setupViews() {
        backgroundColor = UserManager.currentThemeConfiguration.mainColor
        addSubview(logoLabel)
        addSubview(appNameLabel)
        
        logoLabel.snp.makeConstraints { (make) in
            make.top.equalTo(snp.top).offset(20)
            make.left.equalTo(snp.left).offset(20)
        }
        
        appNameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(logoLabel.snp.bottom).offset(4)
            make.left.equalTo(logoLabel.snp.left)
        }
        
    }
    
}
