//
//  ThemeConfigurationFactory.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 5/9/19.
//  Copyright © 2019 Yerassyl Zhassuzakhov. All rights reserved.
//

import Foundation

class ThemeConfigurationFactory {
    func makeThemeConfiguration(for theme: Theme) -> ThemeConfiguration {
        switch theme {
        case .blue:
            return BlueThemeConfigurationImpl()
        case .orange:
            return OrangeThemeConfigurationImpl()
        case .green:
            return GreenThemeConfigurationImpl()
        case .purple:
            return PurpleThemeConfigurationImpl()
        }
    }
}
