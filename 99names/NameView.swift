//
//  NameView.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/15/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class NameView: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        layer.borderColor = UserManager.currentThemeConfiguration.borderColor.cgColor
        layer.borderWidth = 1.4
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let blurView: UIView = {
        let view = UIView()
        view.backgroundColor = UserManager.currentThemeConfiguration.mainColor.withAlphaComponent(0.256)
        return view
    }()
    
    let whiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    let arabicNameLabel: UILabel = {
        let label = UILabel()
        label.text = "الرحمن"
        label.font = label.font.withSize(30)
        label.textColor = .white
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Ар-Рахман"
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    let meaningLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = label.font.withSize(17)
        label.textColor = Colors.grayLabel
        return label
    }()
    
    func setupViews() {
        self.image = UIImage(named: "baiken")
        self.layer.cornerRadius = 10
        self.contentMode = .scaleAspectFill
        self.clipsToBounds = true
        
        addSubview(blurView)
        addSubview(whiteView)
        addSubview(arabicNameLabel)
        addSubview(nameLabel)
        addSubview(meaningLabel)
        
        arabicNameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top)
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.bottom.equalTo(whiteView.snp.top)
        }
        
        whiteView.snp.makeConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.bottom.equalTo(self.snp.bottom)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(whiteView.snp.top)
            make.left.equalTo(whiteView.snp.left)
            make.right.equalTo(whiteView.snp.right)
        }
        
        meaningLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nameLabel.snp.bottom).offset(3)
            make.left.equalTo(whiteView.snp.left).offset(10)
            make.right.equalTo(whiteView.snp.right).offset(-10)
            make.bottom.equalTo(whiteView.snp.bottom).offset(-6)
        }
        
        blurView.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top)
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.bottom.equalTo(whiteView.snp.top)
        }
    }

}



























