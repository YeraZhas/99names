//
//  TestSectionCell.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 7/23/17.
//  Copyright © 2017 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

class TestSectionCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        layer.borderWidth = 1.4
        layer.borderColor = UserManager.currentThemeConfiguration.borderColor.cgColor
        layer.masksToBounds = true
        layer.cornerRadius = 10
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        contentView.backgroundColor = .white
        contentView.addSubview(intervalLabel)
        contentView.addSubview(titleLabel)
        contentView.addSubview(beginButton)
        
        intervalLabel.snp.makeConstraints { (make) in
            make.top.equalTo(contentView.snp.top).offset(10)
            make.left.equalTo(contentView.snp.left)
            make.right.equalTo(contentView.snp.right)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(intervalLabel.snp.bottom).offset(5)
            make.left.equalTo(contentView.snp.left)
            make.right.equalTo(contentView.snp.right)
        }
        
        beginButton.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(frame.height * 0.125)
            make.width.equalTo(100)
            make.centerX.equalTo(contentView.snp.centerX)
            make.height.equalTo(44)
        }
        
    }
    
    let intervalLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = label.font.withSize(30)
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    let beginButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("БАСТАУ".localize(), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UserManager.currentThemeConfiguration.paleMainColor
        return button
    }()
    
    
}
