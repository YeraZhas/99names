//
//  ThemeConfiguration.swift
//  99names
//
//  Created by Yerassyl Zhassuzakhov on 5/9/19.
//  Copyright © 2019 Yerassyl Zhassuzakhov. All rights reserved.
//

import UIKit

protocol ThemeConfiguration {
    var mainColor: UIColor { get }
    var mainMaskColor: UIColor { get }
    var labelsColor: UIColor { get }
    var wrongAnswerColor: UIColor { get }
    var rightAnswerColor: UIColor { get }
    var paleMainColor: UIColor { get }
    var secondaryMaskColor: UIColor { get }
    var borderColor: UIColor { get }
}

extension ThemeConfiguration {
    var mainMaskColor: UIColor {
        return mainColor.withAlphaComponent(0.4)
    }
    var labelsColor: UIColor {
        return UIColor.rgb(118, green: 115, blue: 115)
    }
    var wrongAnswerColor: UIColor {
        return UIColor.rgb(228, green: 228, blue: 228)
    }
    
    var borderColor: UIColor {
        return UIColor.rgb(216, green: 216, blue: 216)
    }
}
